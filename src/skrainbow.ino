#include <Adafruit_NeoPixel.h>

#define PIN_YELLOW      7
#define PIN_BLUE        6
#define BUTTON         12
#define NUM_LEDS      119
#define NUM_RAINBOWS    2
#define BRIGHTNESS     30

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip_yellow = Adafruit_NeoPixel(NUM_LEDS, PIN_YELLOW, \
    NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip_blue   = Adafruit_NeoPixel(NUM_LEDS, PIN_BLUE, \
    NEO_GRB + NEO_KHZ800);

/*
   My set up:
   - 2 119 LED long led strips, one with a blue wire (strip_blue) one with a
   yellow wire (strip_yellow).
   - A grounded button to make things interactive inputs.
 */
void setup() {
  pinMode(BUTTON, INPUT);
  strip_yellow.begin();
  strip_blue.begin();
  strip_yellow.show(); // Initialize all pixels to 'off'
  strip_blue.show();   // Initialize all pixels to 'off'
  strip_yellow.setBrightness(BRIGHTNESS);
  strip_blue.setBrightness(BRIGHTNESS);
}

void loop() {
  uint32_t red    = strip_yellow.Color(255,  30,  30),
           green  = strip_yellow.Color( 30, 255,  30),
           blue   = strip_yellow.Color( 50,  50, 255),
           pink   = strip_yellow.Color(255,   0, 255),
           yellow = strip_yellow.Color(255, 255,  40),
           orange = strip_yellow.Color(255, 127,  25),
           purple = strip_yellow.Color(200,   0, 255);
  skrainbow();
  theaterChaseSkrainbow();
  skolorSet(0, 0, 0, NUM_LEDS); // Turn strips off.
  skolorWipe(  red,    orange, false);
  theaterChask(red,    orange, true);
  colourFade(  red,    orange, false);

  skolorWipe(  orange, yellow, true);
  theaterChask(orange, yellow, false);
  colourFade(  orange, yellow, true);

  skolorWipe(  yellow, green,  false);
  theaterChask(yellow, green,  true);
  colourFade(  yellow, green,  false);

  skolorWipe(  green,  blue,   true);
  theaterChask(green,  blue,   false);
  colourFade(  green,  blue,   true);

  skolorWipe(  blue,   purple, false);
  theaterChask(blue,   purple, true);
  colourFade(  blue,   purple, false);

  theaterChask(pink,   pink,   false);

  quarterFill( red,    blue,   yellow, green);
  theaterChaseSkrainbow();
}

/*
   Fills the LED strips in 4 steps by calling skolorSet().
 */
void quarterFill(uint32_t colour1, uint32_t colour2, uint32_t colour3, uint32_t colour4) {
  uint8_t i=0, quarter = (NUM_LEDS / 4) + 1;
  uint32_t colours[] = { colour1, colour2, colour3, colour4 };

  for (; i<4; i++) {
    for (int8_t j=i; j>=0; j--) {
      skolorSet(colours[j], colours[j], quarter * (i-j), quarter * (i-j+1));
    }
    delay(1000);
  }
}

/*
   Set a (sub)section of both strips to a solid colour/off with no delay or effect.
 */
void skolorSet(uint32_t colour1, uint32_t colour2, uint8_t lower_boundry, uint8_t upper_boundry) {
  uint8_t i=lower_boundry;
  for(; i<upper_boundry; i++) {
    strip_yellow.setPixelColor(i, colour1);
    strip_blue.setPixelColor(i, colour2);
  }
  strip_yellow.show();
  strip_blue.show();
}

/* A variation of NeoPixel's theaterChase.
   Theatre-style crawling lights. Turns every third LED on, waits, turns them
   off again. Then goes to the next LED until all LEDs have been turned on at
   least once.
 */
void theaterChask(uint32_t colour1, uint32_t colour2, bool swapColour) {
  uint8_t i, cycle, q;
  uint32_t active_colour1 = colour1, active_colour2 = colour2;
  for (cycle=0; cycle<20; cycle++) {
    for (q=0; q<3; q++) {
      for (i=0; i<NUM_LEDS; i=i+3) {
        strip_yellow.setPixelColor(i+q, active_colour1);
        strip_blue.setPixelColor(i+q, active_colour2);
      }
      strip_yellow.show();
      strip_blue.show();

      delay(50);

      for (int i=0; i<NUM_LEDS; i=i+3) {
        strip_yellow.setPixelColor(i+q, 0);
        strip_blue.setPixelColor(i+q, 0);
      }
    }
    if ((swapColour) && (cycle % 3 == 0)) {
      if (active_colour1 == colour1) {
        active_colour1 = colour2;
        active_colour2 = colour1;
      } else {
        active_colour1 = colour1;
        active_colour2 = colour2;
      }
    }
  }
}

/*
   Swaps colour1 and colour2 on the two strips. Takes num_leds as argument to
   only swap the first num_leds LEDs in case you don't want to swap the whole
   strips.
 */
void colourSwap(bool normal, uint32_t colour1, uint32_t colour2, uint8_t num_leds) {
  uint8_t i=0;
  if (normal) {
    for(; i<num_leds; i++) {
      strip_yellow.setPixelColor(i, colour1);
      strip_blue.setPixelColor(i, colour2);
    }
  } else {
    for(; i<num_leds; i++) {
      strip_yellow.setPixelColor(i, colour2);
      strip_blue.setPixelColor(i, colour1);
    }
  }
}

/*
   Sets a solid colour on both strips and slowly turns the brightness all the
   way down to 0. If swapColour is true, the strip swap colours throughout this
   by calling colourSwap().
 */
void colourFade(uint32_t colour1, uint32_t colour2, bool swapColour) {
  uint8_t brightness = BRIGHTNESS, i;
  // Set all LEDs.
  for(i=0; i<NUM_LEDS; i++) {
    strip_yellow.setPixelColor(i, colour1);
    strip_blue.setPixelColor(i, colour2);
  }

  while(brightness > 0) {
    brightness--;
    if((swapColour) && (brightness % 5 == 0)) {
      colourSwap(brightness % 2 == 0, colour1, colour2, NUM_LEDS);
    }

    strip_yellow.setBrightness(brightness);
    strip_blue.setBrightness(brightness);
    strip_yellow.show();
    strip_blue.show();

    delay(45);
  }
  strip_yellow.setBrightness(BRIGHTNESS);
  strip_blue.setBrightness(BRIGHTNESS);
}

/*
   A variation of NeoPixel's rainbow() function.
   skrainbow() sends "NUM_RAINBOWS" on both LED strips in opposite directions.
   It is an endless loop that can only be interrupted by reading high on
   "BUTTON".
 */
void skrainbow() {
  uint8_t i, j, k, rainbows = NUM_LEDS / NUM_RAINBOWS;
  while(true) {
    for(j=0; j<256; j++) {
      for(i=0; i<NUM_LEDS; i++) {
        strip_yellow.setPixelColor(i, Wheel(((i * 255 / rainbows) + j) & 255));
        k = 255 - i;
        strip_blue.setPixelColor(i, Wheel(((k * 255 / rainbows) + j) & 255));
      }
      strip_yellow.show();
      strip_blue.show();
      if(digitalRead(BUTTON) == 1) return; // Exit when button is pressed.
    }
  }
}

/* A variation of NeoPixel's theaterChaseRainbow.
   Theatre-style crawling lights with rainbow effect.
 */
void theaterChaseSkrainbow() {
  uint8_t i, j, q;
  for (j=0; j<255; j++) { //cycle all 256 colors in the wheel
    for (q=0; q<3; q++) {  //loop 3 times to turn every pixel on at least once.
      for (i=0; i<NUM_LEDS; i=i+3) { //turn every third pixel on
        strip_yellow.setPixelColor(i+q, Wheel(((i+j) % 255)));
        strip_blue.setPixelColor(i+(2-q), Wheel(((i+(255-j)) % 255)));
      }
      strip_yellow.show();
      strip_blue.show();
      delay(50);
      for (i=0; i<NUM_LEDS; i=i+3) { //turn every third pixel off
        strip_yellow.setPixelColor(i+q, 0);
        strip_blue.setPixelColor(i+(2-q), 0);
      }
    }
  }
}

/*
   A variation of NeoPixel's colorWipe() function.
   Fill the dots of both strips one after the other with a colour.
 */
void skolorWipe(uint32_t colour1, uint32_t colour2, bool swapColour) {
  bool normal = true;
  for(uint8_t i=0; i<NUM_LEDS; i = i + 2) {
    strip_yellow.setPixelColor(i, colour1);
    strip_blue.setPixelColor(i, colour2);
    strip_yellow.setPixelColor(i + 1, colour1);
    strip_blue.setPixelColor(i + 1, colour2);

    if((swapColour) && (i % 10 == 0)) {
      normal = !normal;
      colourSwap(normal, colour1, colour2, i);
    }

    strip_yellow.show();
    strip_blue.show();
    delay(50);
  }
}

/*
   From NeoPixel: Input a value 0 to 255 to get a colour value.
   The colours are a transition r - g - b - back to r.
 */
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
    return strip_yellow.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
    return strip_yellow.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
    WheelPos -= 170;
    return strip_yellow.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
