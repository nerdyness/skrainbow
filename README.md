# skrainbow

Arduino code to operate 2x 119 RBG LED (WS2812B) strips.

# using it
## first use
### install and setup "ino"
```bash
sudo pip install ino #install arduino CLI tool
export PYTHONPATH=/usr/local/lib/python2.7/dist-packages:$PYTHONPATH
sudo chown -R $SUDO_USER:users $PYTHONPATH
sudo ln -sf $PATH_TO_ARDUINO_IDE /usr/local/share/arduino/
sudo chown -R $SUDO_USER:users $/usr/local/share/arduino/
```

## every run
```bash
cd skrainbow
git pull
ino build
ino upload
```

# contributing

* Fork it!
* Create a branch `git checkout -b mybranch`
* Improve it && `git commit -am 'improvement'`
* Push your changes upstream `git push origin -u mybranch`
* And submit a pull request!

Thank you :)
